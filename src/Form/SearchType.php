<?php

namespace App\Form;


use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('searchBar', TextType::class, [
                'attr'=> [
                    'class'=> 'form-control'
                ],
                'required'=>false
            ])
            ->add('categories', EntityType::class, [
                'class'=> Category::class,
                'multiple'=> true,
                'expanded'=> true
            ])

            ->add('rangeType', TextType::class, [
                'attr'=> ['id'=> 'rangePrice',
                    'class'=> 'span2',
                    'data-slider-min'=> $options['data']['minMax']['min'],
                    'data-slider-max'=> $options['data']['minMax']['max'],
                    'data-slider-step'=> 5,
                  ]
            ])

            ->add("Rechercher", SubmitType::class, [
                'attr'=> ['class'=>'btn btn-success']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
