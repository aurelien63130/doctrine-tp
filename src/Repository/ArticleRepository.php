<?php
namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ArticleRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function search($filtres){

        $priceMin = explode(',',$filtres['rangeType'])[0];
        $priceMax = explode(',',$filtres['rangeType'])[1];

        $query =
            $this->createQueryBuilder('a')
                ->join('a.category', 'categ');

       if(!empty($filtres['searchBar'])){
            $query->where('a.name LIKE :search')
                ->orWhere('a.description LIKE :search')
                ->orWhere('categ.nom = :search')
                ->setParameter('search', '%'.$filtres['searchBar'].'%');
        }

       if(count($filtres['categories']) != 0){
           $query->andWhere('categ IN (:array)')
               ->setParameter('array', $filtres['categories']);
       }

        $query->andWhere('a.price BETWEEN :min AND :max')
            ->setParameter('min', $priceMin)
            ->setParameter('max', $priceMax);

       return $query->getQuery()->getResult();
    }

    public function getMinMax(){
        return $this->createQueryBuilder('a')
            ->select('MAX(a.price) as max')
            ->addSelect('MIN(a.price) as min')
           ->getQuery()->getOneOrNullResult();
    }
}