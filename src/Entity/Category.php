<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 * @UniqueEntity("nom", message="La catégorie {{ value }} exite déjà")
 */
class Category{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank(message="Ce champ ne peut pas être vide")
     * @Assert\Length(min=3, minMessage="Le nom doit avoir au moins 3 caractères")
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="category")
     */
    private $articles;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="childrens")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity=Category::class, mappedBy="parent", cascade={"remove"})
     */
    private $childrens;

    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->childrens = new ArrayCollection();
    }

    public function __toString(){
        return $this->nom;
    }

    public function getChildrens(){
        return $this->childrens;
    }

    public function addChildren(Category $category){
        if(!$this->childrens->contains($category)){
            $this->childrens->add($category);
            $category->setParent($this);
        }
    }

    public function removeChildren(Category $category){
        if($this->childrens->contains($category)){
            $this->childrens->removeElement($category);
            $category->setParent($this);
        }
    }

    public function getParent(){
        return $this->parent;
    }

    public function setParent(Category  $category){
        $this->parent = $category;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setCategory($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->removeElement($article)) {
            // set the owning side to null (unless already changed)
            if ($article->getCategory() === $this) {
                $article->setCategory(null);
            }
        }

        return $this;
    }
}