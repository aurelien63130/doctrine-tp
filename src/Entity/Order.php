<?php
namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Order{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $totalPrice;

    /**
     * @ORM\OneToMany(targetEntity=ProductOrder::class, mappedBy="order")
     */
    private $productOrders;

    public function __construct(){
        $this->productOrders = new ArrayCollection();
    }

    public function getProductOrders(){
        return $this->productOrders;
    }

    public function addProductOrder(ProductOrder $productOrder){
        if($this->productOrders->contains($productOrder)){
            $this->productOrders->add($productOrder);
        }
    }

    public function removeProductOrder(ProductOrder  $productOrder){
        if($this->productOrders->contains($productOrder)){
            $this->productOrders->removeElement($productOrder);
        }
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    public function getId(){
        return $this->id;
    }

    /**
     * @param mixed $totalPrice
     */
    public function setTotalPrice($totalPrice): void
    {
        $this->totalPrice = $totalPrice;
    }

}