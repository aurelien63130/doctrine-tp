<?php
namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Product {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string")
     */
    private $nom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;



    /**
     * @ORM\OneToMany(targetEntity=ProductOrder::class, mappedBy="product")
     */
    private $productOrders;

    public function __construct()
    {
        $this->productOrders = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    public function addProductOrder(ProductOrder $productOrder){
        if(!$this->productOrders->contains($productOrder)){
            $this->productOrders->add($productOrder);
            $productOrder->setProduct($this);
        }
    }

    public function getProductOrders(){
        return $this->productOrders;
    }

    public function removeProductOrder(ProductOrder $productOrder){
        if($this->productOrders->contains($productOrder)){
            $this->productOrders->removeElement($productOrder);
            $productOrder->setProduct(null);
        }
    }

    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }


}