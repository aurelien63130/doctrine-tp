<?php
namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/category", name="category_")
 * @IsGranted("ROLE_USER")
 */
class CategoryController extends AbstractController {

    /**
     * @Route("supprimer/{category}", name="delete")
     */
    public function remove(Category $category, EntityManagerInterface $em){
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute("category_getall");
    }
    /**
     * @Route("/edit/{category}", name="edit")
     */
    public function edit(EntityManagerInterface $em, Request $request, Category $category){
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em->flush();
            return $this->redirectToRoute("category_getall");
        }
        return $this->render("category/edit.html.twig", [
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("/add", name="add")
     */
    public function add(EntityManagerInterface $em, Request $request){
        $form = $this->createForm(CategoryType::class, new Category());
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $category = $form->getData();
            $em->persist($category);
            $em->flush();
            $this->addFlash('success', 'Félicitation ! Tu as créé une categorie !');
            return $this->redirectToRoute('category_getall');
        }
        return $this->render('category/add.html.twig',
        [
            'form'=> $form->createView()
        ]);
    }

    /**
     * @Route("", name="getall")
     */
    public function getAll(CategoryRepository $categoryRepository){
        $categs = $categoryRepository->findAll();

        return $this->render('category/list.html.twig', [
            'categories'=> $categs
        ]);
    }

    /**
     * @Route("/{category}", name="detail", requirements={"category"="\d+"})
     */
    public function getOne(Category $category){
        return $this->render("category/detail.html.twig", [
            'categ'=> $category
        ]);
    }
}