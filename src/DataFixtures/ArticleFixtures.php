<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ArticleFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $image = new Image();
        $image->setUrl("croquette-chien.jpeg");
        $image->setTitle('Croquettes pour chien');
        $image->setAlt("Croquettes pour chien");

        $manager->persist($image);

        $image2 = new Image();
        $image2->setUrl("croquette-chat.jpeg");
        $image2->setTitle('Croquettes pour chat');
        $image2->setAlt("Croquettes pour chat");

        $manager->persist($image2);

        $image3 = new Image();
        $image3->setUrl("laisse-chien.jpeg");
        $image3->setTitle('Laisse pour chien');
        $image3->setAlt("Laisse pour chien");

        $manager->persist($image3);



        $article1 = new Article();
        $article1->setName('Croquettes pour chien');
        $article1->setDescription("On est trop fort");
        $article1->setDateAjout(new \DateTime());
        $article1->setImage($image);
        $article1->setPrice(1);
        $article1->setCategory($this->getReference("category_chien"));

        $article2 = new Article();
        $article2->setName('Croquettes pour chat');
        $article2->setDateAjout(new \DateTime());
        $article2->setImage($image2);
        $article2->setPrice(1000);
        $article2->setCategory($this->getReference("category_chat"));

        $article3 = new Article();
        $article3->setName('Laisse pour chien');
        $article3->setDateAjout(new \DateTime());
        $article3->setPrice(2000);
        $article3->setImage($image3);
        $article3->setCategory($this->getReference("category_chien"));



        $articles = [
            $article1,
            $article2,
            $article3
        ];


        foreach ($articles as $article){
            $manager->persist($article);
        }


        $manager->flush();
    }

    public function getDependencies()
    {
        return [CategoryFixtures::class];
    }

}
