<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1->setNom('Chien');
        $manager->persist($category1);
        $this->addReference("category_chien", $category1);

        $category2 = new Category();
        $category2->setNom("Chat");
        $manager->persist($category2);
        $this->addReference("category_chat", $category2);

        $manager->flush();
    }
}
