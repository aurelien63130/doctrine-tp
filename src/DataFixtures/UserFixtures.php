<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $encoder;
    public function __construct(UserPasswordHasherInterface $encoder){
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setEmail('admin@admin.admin');
        $user1->setFirstName("admin");
        $user1->setLastname("admin");
        $user1->setPassword($this->encoder->hashPassword($user1, 'admin'));
        $user1->setRoles(['ROLE_ADMIN']);

        $manager->persist($user1);

        $user1 = new User();
        $user1->setEmail('user@user.user');
        $user1->setFirstName("user");
        $user1->setLastname("user");
        $user1->setPassword($this->encoder->hashPassword($user1, 'user'));

        $manager->persist($user1);

        $manager->flush();
    }
}
